document.addEventListener('DOMContentLoaded', function() {
    var object2 = document.getElementById("object1"); //получаем элемент object
    console.log(object2);
    var svgDocument;
    object2.addEventListener("load",function(){
        svgDocument = object2.contentDocument; //получаем svg элемент внутри object
        console.log(svgDocument);
        var svgElement = svgDocument.getElementById("Ellipse 167 (Stroke)_3"); //получаем любой элемент внутри svg
        console.log(svgElement);
        svgElement.setAttribute("fill", "red"); //меняем атрибуты выбранного элемента
    }, false);

});